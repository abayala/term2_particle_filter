/*
 * particle_filter.cpp
 *
 *  Created on: Dec 12, 2016
 *      Author: Tiffany Huang
 */

#include <random>
#include <algorithm>
#include <iostream>
#include <numeric>
#include <math.h> 
#include <iostream>
#include <sstream>
#include <string>
#include <iterator>
#include "particle_filter.h"

#include <vector>
#include<map>
using namespace std;

void ParticleFilter::init(double x, double y, double theta, double std[]) {
	// TODO: Set the number of particles. Initialize all particles to first position (based on estimates of 
	//   x, y, theta and their uncertainties from GPS) and all weights to 1. 
	// Add random Gaussian noise to each particle.
	// NOTE: Consult particle_filter.h for more information about this method (and others in this file).
    default_random_engine gen; /*create object of random number generaiton engine */
    /*Init number of particles*/
    num_particles = 20;
    // Create a normal (Gaussian) distribution for x around the gps hint with std deviation input
    normal_distribution<double> dist_x(x, std[0]);
    // Create a normal (Gaussian) distribution for y around the gps hint with std deviation input
    normal_distribution<double> dist_y(y, std[1]);
    // Create a normal (Gaussian) distribution for theta around the gps hint with std deviation input
    normal_distribution<double> dist_theta(theta, std[2]);
    particles.resize ( num_particles );
    for (int i = 0; i < num_particles; ++i) {
        Particle* ptemp = &particles[i];
        /*smaple from distribs*/
        ptemp->x = dist_x(gen);
        ptemp->y = dist_y(gen);
        ptemp->theta = dist_theta(gen);
        
    }
    /*Init weights*/
    weights.resize(num_particles);
    std::fill(weights.begin(), weights.end(),1);


}

void ParticleFilter::prediction(double delta_t, double std_pos[], double velocity, double yaw_rate) {
	// TODO: Add measurements to each particle and add random Gaussian noise.
	// NOTE: When adding noise you may find std::normal_distribution and std::default_random_engine useful.
	//  http://en.cppreference.com/w/cpp/numeric/random/normal_distribution
	//  http://www.cplusplus.com/reference/random/default_random_engine/
    default_random_engine gen; /*create object of random number generaiton engine */
    // Create a normal (Gaussian) distribution for x around  std deviation input
    normal_distribution<double> dist_x(0, std_pos[0]);
    // Create a normal (Gaussian) distribution for y around std deviation input
    normal_distribution<double> dist_y(0, std_pos[1]);
    // Create a normal (Gaussian) distribution for theta around  std deviation input
    normal_distribution<double> dist_theta(0, std_pos[2]);
    /*Traverse all particles and update with vehicle model update*/
    for (size_t i = 0; i < particles.size(); i++)
    {
        Particle* curr_part = &particles.at(i);
        double new_x, new_y, new_theta;
        /*Compute new particle without noise*/
        compute_delta_egomotion(delta_t, velocity, yaw_rate, curr_part->x, curr_part->y, curr_part->theta,
            &new_x, &new_y, &new_theta);
        /*Add noise*/
        new_x += dist_x(gen);
        new_y += dist_y(gen);
        new_theta += dist_theta(gen);
        /*Update particle values*/
        curr_part->x = new_x;
        curr_part->y = new_y;
        curr_part->theta = new_theta;
    }
}

std::vector<int> ParticleFilter::dataAssociation( const std::vector<Map::single_landmark_s>* map_landmarks, const std::vector<LandmarkObs>* observations) {
	// TODO: Find the predicted measurement that is closest to each observed measurement and assign the 
	//   observed measurement to this particular landmark.
	// NOTE: this method will NOT be called by the grading code. But you will probably find it useful to 
	//   implement this method and use it as a helper during the updateWeights phase.
    /*Traverse predicted particles*/
    std::vector<int> output( observations->size ( ) );
    for (size_t index_obs = 0; index_obs < observations->size(); index_obs++)
    {
        const LandmarkObs* curr_obs = &observations->at(index_obs);
        /*map to save ordered the sqerr*/
        std::map <double , size_t > distances;
        /*Traverse landmarks array*/
        for (size_t index_landmark = 0; index_landmark < map_landmarks->size(); index_landmark++)
        {
            /*Compute error*/
            double sqerr = std::sqrt ( std::pow( map_landmarks->at(index_landmark).x_f - curr_obs->x, 2) + std::pow ( map_landmarks->at ( index_landmark ).y_f - curr_obs->y , 2 )  );
            /*store the index of the landmar in the error key*/
            distances [ sqerr ] = index_landmark;
        }
        /*store index in landmark array associated to this observation*/
        output.at ( index_obs) = distances.begin()->second;

    }    
    return output;

}

void ParticleFilter::updateWeights(double sensor_range, double std_landmark[], 
		const std::vector<LandmarkObs> &observations, const Map &map_landmarks) {
	// TODO: Update the weights of each particle using a mult-variate Gaussian distribution. You can read
	//   more about this distribution here: https://en.wikipedia.org/wiki/Multivariate_normal_distribution
	// NOTE: The observations are given in the VEHICLE'S coordinate system. Your particles are located
	//   according to the MAP'S coordinate system. You will need to transform between the two systems.
	//   Keep in mind that this transformation requires both rotation AND translation (but no scaling).
	//   The following is a good resource for the theory:
	//   https://www.willamette.edu/~gorr/classes/GeneralGraphics/Transforms/transforms2d.htm
	//   and the following is a good resource for the actual equation to implement (look at equation 
	//   3.33
	//   http://planning.cs.uiuc.edu/node99.html
    std::vector<LandmarkObs> observations_map_frame ( observations.size ( ) );
    for ( size_t idx_particle = 0; idx_particle < particles.size(); idx_particle++ )
    {
       
        for ( size_t i = 0; i < observations.size ( ); i++ )
        {
            transform_to_map ( &observations.at ( i ) , &particles.at(idx_particle),&observations_map_frame[i] );
        }
        std::vector<int> associated_landmarks;
       
        /*Associate map frame observations of particle to nearest landmar in the map */
       associated_landmarks =  dataAssociation (&map_landmarks.landmark_list, &observations_map_frame );
        double sig_x = std_landmark [ 0 ];
        double sig_y = std_landmark [ 1 ];
        /*total weight of particle accumulator*/
        double accum_weight = 1.0;
        for ( size_t idx_associations = 0; idx_associations < associated_landmarks.size(); idx_associations++ )
        {
            // calculate normalization term

            double gauss_norm = ( 1 / ( 2 * M_PI * sig_x * sig_y ) );
            //calculate exponent
            double errx = pow ( observations_map_frame [ idx_associations ].x - ( double ) map_landmarks.landmark_list.at ( associated_landmarks [ idx_associations ] ).x_f , 2 );
            double erry = pow ( observations_map_frame [ idx_associations ].y - ( double ) map_landmarks.landmark_list.at ( associated_landmarks [ idx_associations ] ).y_f , 2 );
            double exponent = ( errx )/ ( 2 * sig_x*sig_x ) + ( erry ) / ( 2 * sig_y*sig_y );
            // calculate weight using normalization terms and exponent and multiply to accumulated weight
            accum_weight = accum_weight * (gauss_norm * exp ( -exponent ));
        }
        weights.at ( idx_particle ) = accum_weight;
    }    

}

void ParticleFilter::resample() {
	// TODO: Resample particles with replacement with probability proportional to their weight. 
	// NOTE: You may find std::discrete_distribution helpful here.
	//   http://en.cppreference.com/w/cpp/numeric/random/discrete_distribution
    default_random_engine gen; /*random number generation  enginge*/
    discrete_distribution<int> dis_dist(0,int (weights.size ( )) );

    std::vector<double>::iterator it = std::max_element ( weights.begin ( ) , weights.end ( ) );
    double max_weight = *it;
    double beta = 0.0;
    std::vector<Particle> out_particles ( weights.size ( ) );
    int index = int ( dis_dist ( gen ) );
    for ( size_t i = 0; i < weights.size ( ); i++ )
    {
        beta = beta + dis_dist ( gen ) * 2.0 * max_weight;
        while ( beta > weights[index] )
        {
            beta -= weights [ index ];
            index = ( index + 1 ) % weights.size ( );
        }
        out_particles.at ( i ) = particles.at ( index );
    }
    particles = out_particles;

}

void ParticleFilter::SetAssociations(Particle& particle, const std::vector<int>& associations, 
                                     const std::vector<double>& sense_x, const std::vector<double>& sense_y)
{
    //particle: the particle to assign each listed association, and association's (x,y) world coordinates mapping to
    // associations: The landmark id that goes along with each listed association
    // sense_x: the associations x mapping already converted to world coordinates
    // sense_y: the associations y mapping already converted to world coordinates

    particle.associations= associations;
    particle.sense_x = sense_x;
    particle.sense_y = sense_y;
}

string ParticleFilter::getAssociations(Particle best)
{
	vector<int> v = best.associations;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<int>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseX(Particle best)
{
	vector<double> v = best.sense_x;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}
string ParticleFilter::getSenseY(Particle best)
{
	vector<double> v = best.sense_y;
	stringstream ss;
    copy( v.begin(), v.end(), ostream_iterator<float>(ss, " "));
    string s = ss.str();
    s = s.substr(0, s.length()-1);  // get rid of the trailing space
    return s;
}

void ParticleFilter::compute_delta_egomotion(double delta_t, double speed_magnitude, double yaw_rate, double  x_in, double  y_in, double  theta_in,
    double * delta_x_out, double * delta_y_out, double * delta_theta_out)
{ 
    double delta_yaw = yaw_rate * delta_t;
    *delta_x_out = x_in + (speed_magnitude / yaw_rate) * (sin(theta_in + delta_yaw) - sin(theta_in));
    *delta_y_out = y_in + (speed_magnitude / yaw_rate) * (cos(theta_in ) - cos(theta_in + delta_yaw));
    *delta_theta_out = theta_in + delta_yaw;
}

void ParticleFilter::transform_to_map ( const LandmarkObs * in_landmark_car_frame, const Particle * const in_particle, LandmarkObs * out_landmark )
{
    /*Init output*/
    *out_landmark = *in_landmark_car_frame;
    double map_car_theta = in_particle->theta;
    double map_car_x = in_particle->x;
    double map_car_y = in_particle->y;

    /*transform x */
    out_landmark->x = map_car_x + cos ( map_car_theta ) * in_landmark_car_frame->x - sin ( map_car_theta * in_landmark_car_frame->y );
    out_landmark->y = map_car_y + sin ( map_car_theta ) * in_landmark_car_frame->x + cos ( map_car_theta * in_landmark_car_frame->y );

}
